package ru.duc.battleship.client.temp;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class Example3Client {
    public static void main(String[] args) {
        try {
            Socket s = new Socket("127.0.0.1", 1234);
            Scanner sc = new Scanner(s.getInputStream());
            OutputStream os = s.getOutputStream();
            os.write("ping\n".getBytes());
            System.out.println(sc.nextLine());
            os.write("time\n".getBytes());
            System.out.println(sc.nextLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
