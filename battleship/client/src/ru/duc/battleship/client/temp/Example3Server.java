package ru.duc.battleship.client.temp;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Scanner;

public class Example3Server {
    public static void main(String[] args) {
        try (ServerSocket ss = new ServerSocket(1234)) {
            Socket s = ss.accept();
            System.out.println("connected :" + s.getInetAddress());
            new Thread(new Connection(s)).start();
//            s.getOutputStream().write("hello".getBytes());
//            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class Connection implements Runnable {
        Socket socket;
        OutputStream os;
        Scanner sc;

        public Connection(Socket socket) throws IOException {
            sc = new Scanner(socket.getInputStream());
            os = socket.getOutputStream();
            this.socket = socket;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    if (!sc.hasNextLine()) continue;
                    String request = sc.nextLine();
                    System.out.println("client say " + request);
                    if (request.equals("ping")) os.write("pong\n".getBytes());
                    if (request.equals("time")) os.write((new Date() + "\n").getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
