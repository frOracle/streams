package ru.duc.battleship.client.temp;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Example1 {
    public static void main(String[] args) {
        try {
            System.out.println(calc("pupkin.txt"));
        } catch (FileNotFoundException e) {
            System.out.println("файл не найден");
        } catch (ArithmeticException e) {
            System.out.println("в файле деление на 0");
        }
    }

    public static int calc(String filename) throws FileNotFoundException {
        try (Scanner sc = new Scanner(new File(filename))) {
            int sum = Integer.parseInt(sc.nextLine());
            while (sc.hasNextLine()) {
                sum /= Integer.parseInt(sc.nextLine());
            }
            return sum;
        }
    }
}