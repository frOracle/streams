package ru.duc.battleship.client.temp;

public class Example2 {
    public static void main(String[] args) {
        (new MyThred("hello")).start();
        (new MyThred("world")).start();

        new Thread(new MyIntThred("are you sleep?")).start();
    }

    static class MyThred extends Thread {
        String text;

        public MyThred(String text) {
            this.text = text;
        }

        @Override
        public void run() {
            while (true) {
                System.out.println(text);
                try {
                    Thread.sleep((long) (Math.random() * 100));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class MyIntThred implements Runnable {
        String text;

        public MyIntThred(String text) {
            this.text = text;
        }

        @Override
        public void run() {
            while (true) {
                System.out.println(text);
                try {
                    Thread.sleep((long) (Math.random() * 100));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
