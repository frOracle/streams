package ru.duc.battleship.client;

import com.wizylab.duck2d.Graph;

public class Sea {
    private static final double SPEED = 0.01;
    private double frame = 0;

    public void timer(long l) {
        frame += SPEED * l;
        if (frame > 20) frame = 0;
    }

    public void draw(Graph g) {
        for (int y = 0; y < 10; y++)
            for (int x = 0; x < 8; x++)
                g.putImage("sea/" + (int) frame, x * 110, y * 60);
    }
}