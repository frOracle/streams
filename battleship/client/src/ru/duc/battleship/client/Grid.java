package ru.duc.battleship.client;

import com.wizylab.duck2d.Graph;

import java.awt.*;

public class Grid {
    private static final Color BORDER_COLOR = new Color(0, 0, 0, 100);
    private static final Color BORDER_ACTIVE = new Color(255, 255, 0, 200);
    public boolean active;
    private int x, y;

    public Grid(int x, int y, boolean active) {
        this.active = active;
        this.x = x;
        this.y = y;
    }

    public void draw(Graph g) {
        g.setColor(active ? BORDER_ACTIVE : BORDER_COLOR);
        g.rect(this.x - 2, this.y - 2, this.x + 360, this.y + 360);


//        g.setFillColor(new Color(255, 255, 0, 255));
        for (int y = 0; y < 12; y++)
            for (int x = 0; x < 12; x++) {
                g.setFillColor(new Color(0, 0, 0, x == 0 || y == 0 || x==11 || y ==11 ? 130 : 80));
                g.fillRect(this.x + x * 30, this.y + y * 30,
                        this.x + (x + 1) * 30 - 1, this.y + (y + 1) * 30 - 1);
            }
    }
}