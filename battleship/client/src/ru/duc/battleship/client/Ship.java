package ru.duc.battleship.client;

import com.wizylab.duck2d.Graph;

public class Ship {
    int x = 1, y = 1, dir = 0, type = 2;

    public Ship(int x, int y, int dir, int type) {
        this.x = x;
        this.y = y;
        this.dir = dir;
        this.type = type;
    }

    public void rotate() {
        dir = (dir + 1) % 4;
    }

    public void draw(Graph g) {
        g.putImage("ships/" + dir + "/" + type, 50 + x * 30 - 15, 150 + y * 30 - 15);
    }
}