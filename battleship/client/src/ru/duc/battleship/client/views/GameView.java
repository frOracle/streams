package ru.duc.battleship.client.views;

import com.wizylab.duck2d.*;
import ru.duc.battleship.client.Grid;
import ru.duc.battleship.client.Sea;
import ru.duc.battleship.client.Ship;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.HashSet;

public class GameView implements View {
    public int x = 5, y = 5;
    private boolean turn = true;
    private Sea sea = new Sea();
    private Grid grid1 = new Grid(51 - 30, 151 - 30, false);
    private Grid grid2 = new Grid(451 - 30, 151 - 30, true);
    private HashSet<Point> points1 = new HashSet<>();
    private HashSet<Point> points2 = new HashSet<>();
    private HashSet<Point> points3 = new HashSet<>();
    private HashSet<Point> points4 = new HashSet<>();
    private Ship[] ships = {
            new Ship(6, 0, 3, 3),
            new Ship(3, 0, 2, 2),
            new Ship(1, 4, 0, 2),
            new Ship(0, 0, 1, 1),
            new Ship(9, 2, 0, 1),
            new Ship(8, 8, 0, 1),
            new Ship(6, 5, 0, 0),
            new Ship(3, 8, 1, 0),
            new Ship(5, 8, 2, 0),
            new Ship(0, 9, 3, 0),
    };

    @Override
    public void onShow() {
    }

    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);

        if (Mouse.x() > 451 && Mouse.x() < 750) x = (Mouse.x() - 451) / 30;
        if (Mouse.y() > 151 && Mouse.y() < 450) y = (Mouse.y() - 151) / 30;
        if (Mouse.onClick(MouseButton.LEFT)) {
            System.out.println(x + " - " + y);
//            points1.add(new Point(x, y));
            (Math.random() * 100 > 50 ? points1 : points3).add(new Point(x, y));
            (Math.random() * 100 > 50 ? points2 : points4).add(new Point(x, y));
//            turn = !turn;
//            grid1.active = !turn;
//            grid2.active = turn;
        }

        sea.timer(l);
    }

    @Override
    public void onDraw(Graph g) {
        sea.draw(g);

        grid1.draw(g);
        grid2.draw(g);

        g.setFillColor(new Color(0, 0, 0, 100));
        for (Point p : points1)
            g.fillRect(51 - 30 + (p.x + 1) * 30, 151 - 30 + (p.y + 1) * 30,
                    51 - 30 + (p.x + 2) * 30 - 1, 151 - 30 + (p.y + 2) * 30 - 1);

        g.setFillColor(new Color(0, 0, 0, 100));
        for (Point p : points2)
            g.fillRect(451 - 30 + (p.x + 1) * 30, 151 - 30 + (p.y + 1) * 30,
                    451 - 30 + (p.x + 2) * 30 - 1, 151 - 30 + (p.y + 2) * 30 - 1);

        for (Ship ship : ships) ship.draw(g);

        g.setFillColor(new Color(255, 0, 0, 60));
        for (Point p : points3)
            g.fillRect(51 - 30 + (p.x + 1) * 30, 151 - 30 + (p.y + 1) * 30,
                    51 - 30 + (p.x + 2) * 30 - 1, 151 - 30 + (p.y + 2) * 30 - 1);

        g.setFillColor(new Color(255, 0, 0, 60));
        for (Point p : points4)
            g.fillRect(451 - 30 + (p.x + 1) * 30, 151 - 30 + (p.y + 1) * 30,
                    451 - 30 + (p.x + 2) * 30 - 1, 151 - 30 + (p.y + 2) * 30 - 1);

        if (turn) {
            g.setFillColor(new Color(255, 255, 0, 80));
            g.fillRect(451 - 30 + (x + 1) * 30, 151 - 30 + (y + 1) * 30,
                    451 - 30 + (x + 2) * 30, 151 - 30 + (y + 2) * 30);
        }
    }
}