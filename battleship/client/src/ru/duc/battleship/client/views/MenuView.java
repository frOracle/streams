package ru.duc.battleship.client.views;

import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.Keyboard;
import com.wizylab.duck2d.View;

import java.awt.*;
import java.awt.event.KeyEvent;

public class MenuView implements View {
    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);
    }

    @Override
    public void onDraw(Graph g) {
        g.setBackground(Color.ORANGE);
    }
}