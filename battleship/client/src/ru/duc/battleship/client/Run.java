package ru.duc.battleship.client;

import com.wizylab.duck2d.Game;
import ru.duc.battleship.client.views.*;

public class Run {
    public static void main(String[] args) {
        Game.addView(new LoginView(), new MenuView(), new PrepareView(), new GameView(), new FinishView());
        Game.start(GameView.class);
    }
}