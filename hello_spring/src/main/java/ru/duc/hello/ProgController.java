package ru.duc.hello;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class ProgController {

    @GetMapping("/progs")
    public String hello(HttpServletRequest request) {
        request.setAttribute("name", request.getParameter("name"));
        return "views/progs";
    }

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "/progs/calc")
    public String calc(HttpServletRequest request) {
        String res = "Error";
        String sa = request.getParameter("a");
        String sb = request.getParameter("b");
        String sz = request.getParameter("z");
        try {
            double a = Double.parseDouble(sa);
            double b = Double.parseDouble(sb);
            int z = Integer.parseInt(sz);
            if (z == 0) res = String.valueOf(a + b);
            if (z == 1) res = String.valueOf(a - b);
            if (z == 2) res = String.valueOf(a * b);
            if (z == 3 && b != 0) res = String.valueOf(a / b);
            request.setAttribute("z", z);
        } catch (NullPointerException | NumberFormatException e) {
            request.setAttribute("z", 0);
        }
        request.setAttribute("a", sa);
        request.setAttribute("b", sb);
        request.setAttribute("res", res);
        return "views/progs/calc";
    }

    @GetMapping("/progs/matrix")
    public String matrix(Map model) {
        int arr[][] = new int[10][10];
        for (int y = 0; y < 10; y++)
            for (int x = 0; x < 10; x++) arr[y][x] = 1;

        for (int y = 2; y < 8; y++)
            for (int x = 2; x < 8; x++) arr[y][x] = 4;

        for (int i = 0; i < 10; i++) arr[i][i] = 2;
        for (int i = 0; i < 10; i++) arr[i][9 - i] = 2;

        arr[0][4] = 3;
        arr[0][5] = 3;
        arr[9][4] = 3;
        arr[9][5] = 3;
        arr[4][0] = 3;
        arr[5][0] = 3;
        arr[4][9] = 3;
        arr[5][9] = 3;

        model.put("arr", arr);
        return "views/progs/matrix";
    }

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.POST}, value = "/progs/weight")
    public String weight(HttpServletRequest request) {
        String sa = request.getParameter("a");
        String sb = request.getParameter("b");
        try {
            int r = Integer.parseInt(sa);
            int w = Integer.parseInt(sb);
            request.setAttribute("res", w - (r - 120));
        } catch (NullPointerException | NumberFormatException e) {
        }
        request.setAttribute("a", sa);
        request.setAttribute("b", sb);
        return "views/progs/weight";
    }
}