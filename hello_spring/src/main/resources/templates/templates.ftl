<#macro base>
    <html>
        <#include "./parts/head.ftl">
        <body>
            <div class="wrapper" style="height:52px;">
                <#include "./parts/header.ftl">
            </div>
            <div class="wrapper">
                <div class="blank"><#nested/></div>
            </div>
            <#--<#include "./parts/footer.ftl">-->
        </body>
    </html>
</#macro>

<#macro blank>
    <@base>
        <div style="padding:20px;"><#nested/></div>
    </@base>
</#macro>

<#macro theory>
    <@base>
        <div class="sidebar">
            <a href="#">Страница 1</a>
            <a href="#">Страница 2</a>
            <a href="#">Страница 3</a>
        </div>
        <div class="content"><#nested/></div>
    </@base>
</#macro>

<#macro progs>
    <@base>
        <div class="sidebar">
            <a href="/progs/weight" title="Программа для определения идеального веса">Идеальный вес</a>
            <a href="/progs/calc">Калькулятор</a>
            <a href="/progs/matrix">Матрица</a>
        </div>
        <div class="content"><#nested/></div>
    </@base>
</#macro>

<#macro games>
    <@base>
        <div class="sidebar">
            <a href="#">Игра 1</a>
            <a href="#">Игра 2</a>
            <a href="#">Игра 3</a>
        </div>
        <div class="content"><#nested/></div>
    </@base>
</#macro>

<#macro tests>
    <@base>
        <div class="sidebar">
            <a href="#">Test 1</a>
            <a href="#">Test 2</a>
            <a href="#">Test 3</a>
        </div>
        <div class="content"><#nested/></div>
    </@base>
</#macro>