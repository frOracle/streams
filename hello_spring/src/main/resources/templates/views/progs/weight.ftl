<#import "../../templates.ftl" as templates>
<@templates.progs>
    <h2>Определи свой идельный вес</h2>
    <form method="post" action="/progs/weight" style="text-align: center">
        <#if res??>
            <#if res==0>
                <label>У вас идеальный вес!</label><br>
            <#else>
                <label>Вам нужно ${(res<0)?then("набрать", "сбросить")} ${res?abs} кг</label><br>
            </#if>
        <#else>
            <label>Заполните форму и узнаете что вам делать!</label><br>
        </#if>
        <label>Ваш рост: <input name="a" type="number" value="${a!}"/></label>
        <label>Ваш вес: <input name="b" type="number" value="${b!}"/></label>
        <br><br>
        <button style="width:200px; height:40px; font-size:30px;">вычислить</button>
    </form>
</@templates.progs>