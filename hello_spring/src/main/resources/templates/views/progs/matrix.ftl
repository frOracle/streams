<#import "../../templates.ftl" as templates>

<style>
    .t {
        display:inline-block;
        width:40px; height:40px;
        background:#f00;
        border-radius:4px;
        margin: 1px;
        color:#fff;
        line-height:40px;
        text-align:center;
        font-size:30px;
        border: 1px solid #333;
    }
    .t1 {background:#ff0; color:#f00;}
    .t2 {background:#f00;}
    .t3 {background:#0f0;}
    .t4 {background:#00f;}
</style>

<@templates.progs>
    <h2>Матрица</h2>
    <#list 0..9 as y>
        <#list 0..9 as x><div class='t t${arr[y][x]}'>${arr[y][x]}</div></#list>
        <br>
    </#list>
</@templates.progs>