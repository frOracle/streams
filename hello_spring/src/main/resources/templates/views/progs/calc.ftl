<#import "../../templates.ftl" as templates>
<@templates.progs>
    <h2>Калькулятор</h2>
    <form method="post" action="/progs/calc">
        <input name="a" type="number" value="${a!}"/>
        <select name="z">
            <option value="0" ${(z?? && z==0)?then('selected','')}>+</option>
            <option value="1" ${(z?? && z==1)?then('selected','')}>-</option>
            <option value="2" ${(z?? && z==2)?then('selected','')}>*</option>
            <option value="3" ${(z?? && z==3)?then('selected','')}>/</option>
        </select>
        <input name="b" type="number" value="${b!}" />
        <label> = ${res!}</label>
        <br><br>
        <button style="width:200px; height:70px; font-size:30px;">вычислить</button>
    </form>
</@templates.progs>