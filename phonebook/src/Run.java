import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Run {
    private static final String FILENAME = "storage.txt";
    private static Scanner sc = new Scanner(System.in);
    private static List<Record> DATA;

    public static void main(String[] args) {
        load();
        while (true) {
            System.out.println("1 - Вывести справочник");
            System.out.println("2 - Добавить новую запись");
            System.out.println("3 - Выход");
            String req = sc.nextLine();
            if (req.equals("1")) {
                showAll();
            } else if (req.equals("2")) {
                addRecord();
            } else if (req.equals("3")) {
                System.out.println("Пока-пока!");
                System.exit(0);
            } else {
                System.out.println("404 Page not found!\nОбратитесь к администратору чтобы он научил вас пользовваться клавиатурой!");
            }
        }
    }

    public static void showAll() {
        System.out.println("СПРАВОЧНИК:");
        System.out.println("------------------------------------");
        for (int i = 0; i < DATA.size(); i++) {
            Record record = DATA.get(i);
            System.out.println("Запись №" + (i + 1));
            System.out.println("   Имя:      " + record.getName());
            System.out.println("   Телефон: 2" + record.getPhone());
            System.out.println("   Создан:   " + record.getCreated());
            System.out.println("------------------------------------");
        }
    }

    public static void addRecord() {
        System.out.println("НОВАЯ ЗАПИСЬ:");
        System.out.println("ВВедите имя:");
        String name = sc.nextLine();
        System.out.println("ВВедите телефон:");
        String phone = sc.nextLine();
        DATA.add(new Record(name, phone));
        save();
        System.out.println("Запись добавлена!");
    }

    public static void save() {
        try (FileOutputStream fos = new FileOutputStream(FILENAME);
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(DATA);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void load() {
        try (FileInputStream fis = new FileInputStream(FILENAME);
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            DATA = (ArrayList<Record>) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            DATA = new ArrayList<>();
            e.printStackTrace();
        }
    }
}