import java.io.Serializable;
import java.util.Date;

public class Record implements Serializable {
    private String name;
    private String phone;
    private Date created;

    public Record(String name, String phone) {
        this.name = name;
        this.phone = phone;
        this.created = new Date();
    }

    public Record(String name, String phone, Date created) {
        this.name = name;
        this.phone = phone;
        this.created = created;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}