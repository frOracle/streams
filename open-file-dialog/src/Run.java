import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.Keyboard;
import com.wizylab.duck2d.View;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Run implements View {
    private int selected = 0;
    private File[] files;

    public static void main(String[] args) {
        Game.start(new Run());
    }

    @Override
    public void onShow() {
        files = new File("maps").listFiles();
    }

    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);

        if (Keyboard.onKey(KeyEvent.VK_UP))
            selected = selected - 1 < 0 ? files.length - 1 : selected - 1;
        if (Keyboard.onKey(KeyEvent.VK_DOWN))
            selected = selected + 1 >= files.length ? 0 : selected + 1;

        if (Keyboard.onKey(KeyEvent.VK_ENTER)) {
            File file = new File(files[selected].getAbsolutePath());
            System.out.println("----------------------------------------");
            System.out.println("OPEN: " + file.getName().toUpperCase());
            System.out.println("----------------------------------------");
            try (Scanner sc = new Scanner(file)) {
                while (sc.hasNextLine()) System.out.println(sc.nextLine());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDraw(Graph g) {
        g.setFillColor(Color.GREEN);
        g.fillRect(100, 100 + selected * 50, 500, 150 + selected * 50);

        g.setColor(Color.YELLOW);
        g.setTextStyle(1, 1, 24);
        for (int i = 0; i < files.length; i++)
            g.ctext(100, 100 + i * 50, 500, 150 + i * 50, files[i].getName());
    }
}
