import com.wizylab.duck2d.*;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.io.File;

public class OpenDialogExample implements View {
    public static void main(String[] args) {
        Game.start(new OpenDialogExample());
    }

    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);

        if (Keyboard.onKey(KeyEvent.VK_ENTER)) {
            JFileChooser fc = new JFileChooser();
            fc.setMultiSelectionEnabled(true);
            int returnVal = fc.showOpenDialog(Window.instance());
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File[] files = fc.getSelectedFiles();
                for (File f : files) System.out.println("Opening: " + f.getName());
            } else {
                System.out.println("Open command cancelled by user.");
            }
        }

    }

    @Override
    public void onDraw(Graph graph) {
    }
}
