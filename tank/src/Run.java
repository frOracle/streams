import com.wizylab.duck2d.*;

import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Set;

public class Run implements View {
    private double lastTrack = 0, sx = 0, sy = 0;
    private Set<Bullet> bullets = new HashSet<>();
    private Player player = new Player(100, 100);
    private Player tank = new Player(200, 200);

    public static void main(String[] args) {
        Game.start(new Run());
    }

    @Override
    public void onShow() {
        bullets.add(new Bullet(200, 200, 0));
    }

    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);

        int move = 0, angle = 0;
        if (Keyboard.hasKey(KeyEvent.VK_A)) angle++;
        if (Keyboard.hasKey(KeyEvent.VK_D)) angle--;
        if (Keyboard.hasKey(KeyEvent.VK_W)) move++;
        if (Keyboard.hasKey(KeyEvent.VK_S)) move--;

        player.move(l, move);
        player.rotateBody(l, angle);
        player.rotateGun(l, Mouse.x() + sx, Mouse.y() + sy);

        if (Mouse.hasClick(MouseButton.LEFT))
            bullets.addAll(player.fire());

        Set<Bullet> toDelete = new HashSet<>();
        for (Bullet bullet : bullets) {
            bullet.timer(l);
            if (bullet.isFree()) toDelete.add(bullet);
        }
        bullets.removeAll(toDelete);

        sx = player.getX() - 400;
        sy = player.getY() - 300;
    }

    @Override
    public void onDraw(Graph g) {
        for (int x = 0; x < 10; x++)
            for (int y = 0; y < 10; y++)
                g.putImage("ground/sand", x * 128 - sx, y * 128 - sy);

        for (Bullet bullet : bullets) bullet.draw(g, sx, sy);
        player.draw(g, sx, sy);
        tank.draw(g, sx, sy);
    }
}