import com.wizylab.duck2d.Graph;

public class Bullet {
    private static final double SPEED = 0.3;
    private double x, y, angle, time;

    public Bullet(double x, double y, double angle) {
        this.angle = angle;
        this.time = 0;
        this.x = x;
        this.y = y;
    }

    public void timer(long ms) {
        if (isFree()) return;
        this.x += SPEED * ms * Math.cos(angle);
        this.y += SPEED * ms * Math.sin(angle);
        this.time += ms;
    }

    public boolean isFree() {
        return time > 3000;
    }

    public void draw(Graph g, double sx, double sy) {
        if (!isFree()) g.putRotateImage("rocket", angle, x - 8 - sx, y - 8 - sy);
    }
}