import com.wizylab.duck2d.Graph;

import java.util.HashSet;
import java.util.Set;

public class Player {
    private static final double GRAVITY = 0.00005, MOVE_SPEED_TANK = 0.1, ROTATE_SPEED_BODY = 0.002, ROTATE_SPEED_GUN = 0.005;
    private static final double FIRE_INTERVAL = 1000;
    private double x, y, velocity = 0, bodyAngle = 0, gunAngle = 0;
    private long lastFire = 0;

    public Player(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Set<Bullet> fire() {
        HashSet<Bullet> bullets = new HashSet<>();
        long time = System.currentTimeMillis();
        if (time - lastFire < FIRE_INTERVAL) return bullets;
        double x = this.x + 35 * Math.cos(gunAngle);
        double y = this.y + 35 * Math.sin(gunAngle);
        bullets.add(new Bullet(x, y, gunAngle));
        lastFire = time;
        return bullets;
    }

    public void move(long ms, int dir) {
        double oldGravity = velocity;
        long mms = (velocity != 0) ? (long) -(velocity / Math.abs(velocity) * ms) : 0;
        velocity += GRAVITY * (dir != 0 ? ms * dir : mms);
        if (velocity > MOVE_SPEED_TANK) velocity = MOVE_SPEED_TANK;
        if (velocity < -MOVE_SPEED_TANK) velocity = -MOVE_SPEED_TANK;
        if ((dir == 0) && ((oldGravity < 0 && velocity > 0) || (oldGravity > 0 && velocity < 0))) velocity = 0;
        this.x += velocity * Math.cos(bodyAngle);
        this.y += velocity * Math.sin(bodyAngle);
    }

    public void rotateBody(long ms, int dir) {
        bodyAngle -= ROTATE_SPEED_BODY * ms * dir;
    }

    public void rotateGun(long ms, double tx, double ty) {
        // угол к которому идем (+-360) \ растояние со знаком \ растояние без знака
        double a2 = Math.atan2(ty - this.y, tx - this.x), b2 = a2 - gunAngle, c2 = Math.abs(b2);
        double a1 = a2 - 2 * Math.PI, b1 = a1 - gunAngle, c1 = Math.abs(b1);
        double a3 = a2 + 2 * Math.PI, b3 = a3 - gunAngle, c3 = Math.abs(b3);
        // находим лучший угл (к которому идем) и сохраняем знак
        double a;
        if (c1 < c2 && c1 < c3) a = b1;
        else if (c2 < c1 && c2 < c3) a = b2;
        else a = c3;
        // по знаку, определяем направление
        if (a < 0) gunAngle -= ROTATE_SPEED_GUN * ms;
        if (a > 0) gunAngle += ROTATE_SPEED_GUN * ms;
        gunAngle %= 2 * Math.PI;
    }

    public void draw(Graph g, double sx, double sy) {
        g.putRotateImage("shadow", bodyAngle, x - 24 - sx, y - 24 - sy);
        g.putRotateImage("body", bodyAngle, x - 24 - sx, y - 24 - sy);
        g.putRotateImage("gun", gunAngle, x - 33 - sx, y - 33 - sy);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getBodyAngle() {
        return bodyAngle;
    }

    public double getGunAngle() {
        return gunAngle;
    }
}