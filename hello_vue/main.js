/*
let messages = ["123", "hello world", "goodbye", "I'm not vasya"], i = 0;
function abc(e){
	i = ++i % messages.length;
	//if (++i>=messages.length) i = 0;
	document.getElementById('text').innerHTML = messages[i];
	return false;
}
*/
var vm = new Vue({
  el: '#app1', data:{id:0, messages:["123", "hello world", "goodbye", "I'm not vasya"]}	  
});

var vm = new Vue({
  el: '#app2',
  data:{a:1, b:2, zn:0},
  computed: {
	  res(){
		  if (!this.a || !this.b) return "Числа отсутствуют";
		  let a = parseInt(this.a), b = parseInt(this.b);
		  if (this.zn == 0) return a + b;
		  if (this.zn == 1) return a - b;
		  if (this.zn == 2) return a * b;
		  if (this.zn == 3) return b ? a / b : "Делить на 0 нельзя";
		  return "Ошибка";
	  }
  }
});

var vm = new Vue({
  el: '#app3', data:{a:5}
});

var vm = new Vue({el: '#app4'});

var vm = new Vue({
	el: '#app5',
	data:{n:1, a:0, hint:"", errors: 0, active: false},
	mounted(){
		this.reset();
	},
	methods: {
		answer(){
			if (this.n == this.a) {
				this.hint = "Ура! Вы угадали!";
				this.active = false;
				return;
			} 
			if (++this.errors >= 7) {
				this.hint = "Вы проиграли";
				this.active = false;
				return;
			}
			this.hint = "Загаданное число " + (this.n > this.a ? "больше" : "меньше")
		},
		reset(){
			this.hint = "Введите число и нажмите \"ответить\"";
			this.n = Math.random() * 100 >> 0 + 1 
			this.active = true;
			this.errors = 0;
		}
	}
})