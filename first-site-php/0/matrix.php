<html>
	<head>
		<title>Наш супер матрица</title>
		<style>
			body {background:#000;}
			div {
				display:inline-block;
				width:40px; height:40px;
				background:#f00;
				border-radius:4px;
				margin: 1px;
				color:#fff;
				line-height:40px;
				text-align:center;
				font-size:30px;
			}
			.t1 {background:#ff0; color:#f00;}
			.t2 {background:#f00;}
			.t3 {background:#0f0;}
			.t4 {background:#00f;}
		</style>
	</head>
	<body>
		<?php
			$arr = array();
			
			for ($i=0; $i<10; $i++) 
				for ($j=0; $j<10; $j++) 
					$arr[$i][$j] = 1;

			for ($i=2; $i<8; $i++) 
				for ($j=2; $j<8; $j++) 
					$arr[$i][$j] = 4;
				
			for ($i=0; $i<10; $i++) $arr[$i][$i] = 2;
			for ($i=0; $i<10; $i++) $arr[$i][9-$i] = 2;
				
			$arr[0][4] = 3; $arr[0][5] = 3;
			$arr[9][4] = 3; $arr[9][5] = 3;
			$arr[4][0] = 3; $arr[5][0] = 3;
			$arr[4][9] = 3; $arr[5][9] = 3;
			
			for ($i=0; $i<10; $i++) {
				for ($j=0; $j<10; $j++) 
					echo "<div class='t".$arr[$i][$j]."'>".$arr[$i][$j]."</div>";
				echo "<br>";
			}
		?>
	</body>
</html>