const COUNT = 4;
let a = 5, s = "5";
ш = 4*3;
var name = "vasya";

if (a===5) {
	console.log("hello");
} else {
	console.log("bye");
}

for (let i=0;i<10;i++){
	console.log(i);	
}

while(1==2) console.log("i am while");

let arr = [11,122,33];
arr.push(11);
console.log(arr);

let obj = {age:5};
obj.name = "vasya";
obj.height = 130;
console.log(obj);

function calc(a, b){
	return a+b; 
}
console.log(calc(4,3));

let sum = function(a,b) {
	return a+b;
}
console.log(sum(4,3));

let arrow = (a,b) => a+b;
console.log(arrow(4,3));

window.onload = function(){
	let el = document.getElementById("first");
	el.innerHTML = "";
	for (let i = 0; i<10; i++)
		el.innerHTML+=i+" ";
	el.onclick = function(){
		console.log("hello world");
	}
}