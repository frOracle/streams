import com.wizylab.duck2d.*;

import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Set;

public class Run implements View {
    private static final double MOVE_SPEED_TANK = 0.07, ROTATE_SPEED_BODY = 0.002, ROTATE_SPEED_GUN = 0.01;
    private double angleBody = 0, angleGun = 0, disctance = 0, lastTrack = 0;
    private double sx = 0, sy = 0, x = 100, y = 100;
    private Graph trail = new Graph(1280, 1280);
    private Set<Bullet> bullets = new HashSet<>();

    public static void main(String[] args) {
        Game.start(new Run());
    }

    @Override
    public void onShow() {
        bullets.add(new Bullet(200, 200, 0));
    }

    @Override
    public void onTimer(long l) {
        double move = 0;
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);
        if (Keyboard.hasKey(KeyEvent.VK_W)) move += MOVE_SPEED_TANK * l;
        if (Keyboard.hasKey(KeyEvent.VK_S)) move -= MOVE_SPEED_TANK * l;
        if (Keyboard.hasKey(KeyEvent.VK_A)) angleBody -= ROTATE_SPEED_BODY * l;
        if (Keyboard.hasKey(KeyEvent.VK_D)) angleBody += ROTATE_SPEED_BODY * l;
        if (Mouse.onClick(MouseButton.LEFT)) bullets.add(new Bullet(x, y, angleGun));

        disctance += move;

        double mx = Mouse.x() + sx, my = Mouse.y() + sy;
        // угол к которому идем (+-360) \ растояние со знаком \ растояние без знака
        double a2 = Math.atan2(my - this.y, mx - this.x), b2 = a2 - angleGun, c2 = Math.abs(b2);
        double a1 = a2 - 2 * Math.PI, b1 = a1 - angleGun, c1 = Math.abs(b1);
        double a3 = a2 + 2 * Math.PI, b3 = a3 - angleGun, c3 = Math.abs(b3);
        // находим лучший угл (к которому идем) и сохраняем знак
        double a;
        if (c1 < c2 && c1 < c3) a = b1;
        else if (c2 < c1 && c2 < c3) a = b2;
        else a = c3;
        // по знаку, определяем направление
        if (a < 0) angleGun -= ROTATE_SPEED_GUN;
        if (a > 0) angleGun += ROTATE_SPEED_GUN;
        angleGun %= 2 * Math.PI;

        // смещение танка
        this.x += move * Math.cos(angleBody);
        this.y += move * Math.sin(angleBody);

        if (disctance - lastTrack > 20) {
            trail.putRotateImage("track", angleBody, x - 24, y - 24);
            lastTrack = disctance;
        }

        Set<Bullet> toDelete = new HashSet<>();
        for (Bullet bullet : bullets) {
            bullet.timer(l);
            if (bullet.getTime() > 3000)
                toDelete.add(bullet);
        }
        bullets.removeAll(toDelete);


        sx = x - 400;
        sy = y - 300;
    }

    @Override
    public void onDraw(Graph g) {
        for (int x = 0; x < 10; x++)
            for (int y = 0; y < 10; y++)
                g.putImage("ground/sand", x * 128 - sx, y * 128 - sy);

        for (Bullet bullet : bullets) bullet.draw(g, sx, sy);

        g.putGraph(trail, (int) -sx, (int) -sy, (int) (1280 - sx), (int) (1280 - sy), 0, 0, 1280, 1280);
        g.putRotateImage("shadow", angleBody, x - 24 - sx, y - 24 - sy);
        g.putRotateImage("body", angleBody, x - 24 - sx, y - 24 - sy);
        g.putRotateImage("gun", angleGun, x - 33 - sx, y - 33 - sy);
    }
}