import com.wizylab.duck2d.Graph;

public class Bullet {
    private static final double SPEED = 0.3;
    private double x, y, angle, time;

    public Bullet(double x, double y, double angle) {
        this.x = x;
        this.y = y;
        this.angle = angle;
        this.time = 0;
    }

    public void timer(long l) {
        this.x += SPEED * l * Math.cos(angle);
        this.y += SPEED * l * Math.sin(angle);
        this.time += l;
    }

    public double getTime() {
        return time;
    }

    public void draw(Graph g, double sx, double sy) {
        g.putRotateImage("rocket", angle, x - 8 - sx, y - 8 - sy);
    }

}
