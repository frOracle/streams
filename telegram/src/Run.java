import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Run {
    private static final String FILENAME = "buffer.txt";
    private static int COUNT = 0;

    public static void main(String[] args) {
        new Thread(new Writer()).start();
        new Thread(new Reader()).start();
    }

    static class Writer implements Runnable {
        @Override
        public void run() {
            Scanner sc = new Scanner(System.in);
            while (true) {
                String str = sc.nextLine();
                if (str.equals("exit")) System.exit(0);
                try (FileOutputStream fos = new FileOutputStream(FILENAME, true)) {
                    fos.write((str + "\n").getBytes());
                    COUNT++;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class Reader implements Runnable {
        @Override
        public void run() {
            while (true) {
                try (Scanner sc = new Scanner(new File(FILENAME))) {
                    int count = 0;
                    while (sc.hasNextLine()) {
                        String str = sc.nextLine();
                        count++;
                        if (count > COUNT) System.out.println(str);
                    }
                    COUNT = count;
                    Thread.sleep(1000);
                } catch (FileNotFoundException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}