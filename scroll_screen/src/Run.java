import com.wizylab.duck2d.*;
import com.wizylab.duck2d.Window;

import java.awt.*;
import java.awt.event.KeyEvent;

public class Run implements View {
    private static final Color[] TERRAIN = {Color.BLUE, Color.GREEN, new Color(0, 180, 0), new Color(0, 200, 0)};
    private static final double SPEED = 0.1;
    private double x = 100, y = 100, sx = 0, sy = 0;
    private int map[][] = new int[100][100];

    @Override
    public void onShow() {
        for (int x = 0; x < map[0].length; x++)
            for (int y = 0; y < map.length; y++)
                map[y][x] = (int) (TERRAIN.length * Math.random());
    }

    public static void main(String[] args) {
        Game.start(new Run());
    }

    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);
        if (Keyboard.hasKey(KeyEvent.VK_LEFT) && !isWall(x - SPEED * l, y)) x -= SPEED * l;
        if (Keyboard.hasKey(KeyEvent.VK_RIGHT) && !isWall(x + SPEED * l, y)) x += SPEED * l;
        if (Keyboard.hasKey(KeyEvent.VK_UP) && !isWall(x, y - SPEED * l)) y -= SPEED * l;
        if (Keyboard.hasKey(KeyEvent.VK_DOWN) && !isWall(x, y + SPEED * l)) y += SPEED * l;
        sx = x - 400;
        sy = y - 300;
    }

    private boolean isWall(double x, double y) {
        int cx = (int) x / 50, cy = (int) y / 50;
        Window.instance().setTitle(String.format("x=%d; y=%d", cx, cy));

        Rectangle hitbox = new Rectangle((int) x - 20, (int) y - 20, 40, 40);
        int x1 = (int) Math.max(x / 50 - 1, 0), x2 = (int) Math.min(x / 50 + 1, map[0].length - 1);
        int y1 = (int) Math.max(y / 50 - 1, 0), y2 = (int) Math.min(y / 50 + 1, map.length - 1);
        for (int tx = x1; tx <= x2; tx++)
            for (int ty = y1; ty <= y2; ty++) {
                if (map[ty][tx] != 0) continue;
                Rectangle cell = new Rectangle(tx * 50, ty * 50, 50, 50);
                if (hitbox.intersects(cell)) return true;
            }

        return false;
    }

    @Override
    public void onDraw(Graph g) {
        {
            int x1 = (int) Math.max(x / 50 - 8, 0), x2 = (int) Math.min(x / 50 + 8, map[0].length - 1);
            int y1 = (int) Math.max(y / 50 - 6, 0), y2 = (int) Math.min(y / 50 + 6, map.length - 1);
            for (int x = x1; x <= x2; x++)
                for (int y = y1; y <= y2; y++) {
                    g.setFillColor(TERRAIN[map[y][x]]);
                    g.fillRect(x * 50 - (int) sx, y * 50 - (int) sy, (x + 1) * 50 - (int) sx, (y + 1) * 50 - (int) sy);
                }
        }

        {
            g.setColor(Color.RED);
            int x1 = (int) Math.max(x / 50 - 1, 0), x2 = (int) Math.min(x / 50 + 1, map[0].length - 1);
            int y1 = (int) Math.max(y / 50 - 1, 0), y2 = (int) Math.min(y / 50 + 1, map.length - 1);
            for (int tx = x1; tx <= x2; tx++)
                for (int ty = y1; ty <= y2; ty++)
                    g.rect(tx * 50 - (int) sx, ty * 50 - (int) sy, (tx + 1) * 50 - (int) sx, (ty + 1) * 50 - (int) sy);
        }

        g.setFillColor(Color.YELLOW);
        g.fillCircle((int) (x - sx), (int) (y - sy), 20);
    }
}