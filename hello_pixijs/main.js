let app;
const speed = 5;
const keyboard = [];
window.addEventListener("keydown", (event)=> keyboard[event.keyCode] = true);
window.addEventListener("keyup", (event)=> keyboard[event.keyCode] = false);
window.addEventListener("resize", (event)=> app.renderer.resize(window.innerWidth, window.innerHeight));
	
window.onload = function(){
	app = new PIXI.Application({width: window.innerWidth, height: window.innerHeight, backgroundColor: 0x1099bb, resolution: window.devicePixelRatio || 1});
	document.body.appendChild(app.view);

	const bunny = new PIXI.Sprite.from('bunny.png');
	bunny.scale.set(2,2);
	bunny.anchor.set(0.5);
	bunny.x = 400;
	bunny.y = 300;
	app.stage.addChild(bunny);

	app.ticker.add((delta) => {
		if (keyboard[37]) bunny.x -= speed * delta;
		if (keyboard[38]) bunny.y -= speed * delta;
		if (keyboard[39]) bunny.x += speed * delta;
		if (keyboard[40]) bunny.y += speed * delta;
	});
}
