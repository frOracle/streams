import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Run {
    public static void main(String[] args) throws IOException {
        ServerSocket ss = new ServerSocket(80);
        while (true) {
            System.out.println("connected");
            proccess(ss.accept());
        }
    }

    private static void proccess(Socket s) throws IOException {
        try (OutputStream os = s.getOutputStream();
             InputStream is = s.getInputStream();
             Scanner sc = new Scanner(is)) {
            StringBuilder request = new StringBuilder();

            while (sc.hasNextLine()) {
                String str = sc.nextLine();
                request.append(str).append("\n");
                if (str.contains("host")) break;
            }

            System.out.println("REQUEST - " + request);

            String query = (request.indexOf("HTTP") >= 0) ?
                    request.substring(request.indexOf("GET") + 3, request.indexOf("HTTP")).trim() : "";
            String responce = String.format(RESPONCE, 404, 0, "");
            if (query.equalsIgnoreCase("/hello"))
                responce = String.format(RESPONCE, 200, HTML.length(), HTML);
            os.write(responce.getBytes());
            System.out.println(request);
        }
    }

    static String HTML = "" +
            "<html>\n" +
            "  <head>\n" +
            "    <title>An Example Page</title>\n" +
            "  </head>\n" +
            "  <body>\n" +
            "    <p>Hello World, this is a very simple HTML document.</p>\n" +
            "  </body>\n" +
            "</html>";

    static String RESPONCE = "" +
            "HTTP/1.1 %d OK\n" +
            "Date: Mon, 23 May 2005 22:38:34 GMT\n" +
            "Content-Type: text/html; charset=UTF-8\n" +
            "Content-Length: %d\n" +
            "Last-Modified: Wed, 08 Jan 2003 23:11:55 GMT\n" +
            "Server: Apache/1.3.3.7 (Unix) (Red-Hat/Linux)\n" +
            "ETag: \"3f80f-1b6-3e1cb03b\"\n" +
            "Accept-Ranges: bytes\n" +
            "Connection: close\n" +
            "\n%s";
}