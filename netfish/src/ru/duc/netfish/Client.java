package ru.duc.netfish;

import com.wizylab.duck2d.Game;
import ru.duc.netfish.views.GameView;
import ru.duc.netfish.views.MenuView;

public class Client {
    public static void main(String[] args) {
        Game.addView(new MenuView(), new GameView());
        Game.start(MenuView.class);
    }
}