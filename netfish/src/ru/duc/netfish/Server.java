package ru.duc.netfish;

import ru.duc.netfish.common.Pickup;
import ru.duc.netfish.common.Player;
import ru.duc.netfish.common.SysUtils;
import ru.duc.netfish.common.requests.LoginRequest;
import ru.duc.netfish.common.requests.MoveRequest;
import ru.duc.netfish.common.responses.LoginResponse;
import ru.duc.netfish.common.responses.MoveResponse;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

public class Server {
    private static Set<Player> players = new HashSet<>();
    private static Set<Pickup> pickups = new HashSet<>();

    public static void main(String[] args) {
        try (ServerSocket ss = new ServerSocket(1234)) {
            new Thread(new PickupRefresher()).start();
            System.out.println("Server start");
            while (true) {
                Socket socket = ss.accept();
                System.out.println("client connected");
                new Thread(new Channel(socket)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static class PickupRefresher implements Runnable {
        public static final long INTERVAL = 60000;
        public static final long COUNT = 250;

        @Override
        public void run() {
            while (true) {
                Set<Pickup> tmp = new HashSet<>();
                for (int i = 0; i < COUNT; i++)
                    tmp.add(new Pickup());
                pickups = tmp;
                SysUtils.sleep(INTERVAL);
            }
        }
    }

    static class Channel implements Runnable {
        private Socket socket;
        private ObjectOutputStream oos;
        private ObjectInputStream ois;
        private Player player;

        public Channel(Socket socket) throws IOException {
            this.socket = socket;
            oos = new ObjectOutputStream(socket.getOutputStream());
            ois = new ObjectInputStream(socket.getInputStream());
        }

        @Override
        public void run() {
            try {
                while (true) {
                    Object obj = ois.readObject();
                    if (obj instanceof LoginRequest) {
                        LoginRequest request = (LoginRequest) obj;
                        player = new Player(request.getName(), request.getType());
                        players.add(player);
                        oos.writeObject(new LoginResponse(player.getUUID()));
                        oos.flush();
                    } else if (obj instanceof MoveRequest) {
                        player.move((MoveRequest) obj);
                        for (Player player : players) {
                            if (player == this.player || player.isDead() || this.player.isDead() ||
                                    !this.player.getHitbox().intersects(player.getHitbox())) continue;
                            if (player.getSize() < this.player.getSize()) {
                                this.player.inc(player.getSize() / 3);
                                player.kill();
                            } else {
                                player.inc(this.player.getSize() / 3);
                                this.player.kill();
                            }
                        }

                        for (Pickup pickup : pickups) {
                            if (!pickup.isActive() || !player.getHitbox().intersects(pickup.getHitbox())) continue;
                            player.inc(pickup.getSize() - 9);
                            pickup.pickup();
                        }

                        oos.writeObject(new MoveResponse(players, pickups));
                        oos.flush();
                    }
                }
            } catch (IOException | ClassNotFoundException e) {
                players.remove(player);
                e.printStackTrace();
            } finally {
                SysUtils.close(oos);
                SysUtils.close(ois);
                SysUtils.close(socket);
            }
        }
    }
}