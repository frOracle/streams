package ru.duc.netfish.views;

import com.wizylab.duck2d.*;
import ru.duc.netfish.common.Pickup;
import ru.duc.netfish.common.Player;
import ru.duc.netfish.common.SysUtils;
import ru.duc.netfish.common.requests.LoginRequest;
import ru.duc.netfish.common.requests.MoveRequest;
import ru.duc.netfish.common.responses.LoginResponse;
import ru.duc.netfish.common.responses.MoveResponse;

import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class GameView implements View {
    private Connection connection = new Connection();
    private Map<Long, Player> players;
    private Set<Pickup> pickups;
    private long left, right, up, down;
    private double sx, sy;
    private Long uuid;

    public GameView() {
        new Thread(new Sender()).start();
    }

    @Override
    public void onShow() {
        try {
            uuid = null;
            left = right = up = down = 0;
            players = new HashMap<>();
            pickups = new HashSet<>();

            connection.connect("localhost", 1234);
            if (!connection.active) throw new IOException("Can't connect to server");
            LoginResponse response = (LoginResponse) connection.send(
                    new LoginRequest(
                            Environment.get("name", "Unknown"),
                            Environment.get("type", 0)));
            uuid = response.getUUID();

        } catch (IOException e) {
            e.printStackTrace();
            Game.show(MenuView.class);
        }
    }

    @Override
    public void onClose() {
        connection.close();
        uuid = null;
    }

    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) Game.show(MenuView.class);
        if (Keyboard.hasKey(KeyEvent.VK_LEFT)) left += l;
        if (Keyboard.hasKey(KeyEvent.VK_RIGHT)) right += l;
        if (Keyboard.hasKey(KeyEvent.VK_UP)) up += l;
        if (Keyboard.hasKey(KeyEvent.VK_DOWN)) down += l;
        Player player = players.get(uuid);
        if (player == null) return;
        sx = player.getX() - 400 + player.getSize() / 2;
        sy = player.getY() - 300 + player.getSize() / 2;
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("bg2", 0, 0);
        for (Player player : players.values()) player.draw(g, sx, sy);
        for (Pickup pickup : pickups) pickup.draw(g, sx, sy);

        Player player = players.get(uuid);
        if (player == null) return;
        String title = String.format("x=%d, y=%d", (int) player.getX(), (int) player.getY());
        Window.instance().setTitle(title);
    }

    class Sender implements Runnable {
        @Override
        public void run() {
            while (true) {
                try {
                    SysUtils.sleep(20);
                    if (!connection.active || uuid == null) continue;
                    Object obj = connection.send(new MoveRequest(left, right, up, down));
                    MoveResponse response = (MoveResponse) obj;
                    players = response.getPlayers();
                    pickups = response.getPickups();
                    left = right = up = down = 0;
                } catch (NullPointerException | ClassCastException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class Connection {
        private boolean active = false;
        private Socket socket;
        private ObjectOutputStream oos;
        private ObjectInputStream ois;

        public void connect(String host, int port) {
            try {
                socket = new Socket(host, port);
                oos = new ObjectOutputStream(socket.getOutputStream());
                ois = new ObjectInputStream(socket.getInputStream());
                active = true;
            } catch (IOException e) {
                e.printStackTrace();
                close();
            }
        }

        public Object send(Object obj) {
            try {
                if (!active) throw new IOException("Connection not active");
                oos.writeObject(obj);
                oos.flush();
                return ois.readObject();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }

        public void close() {
            SysUtils.close(ois);
            SysUtils.close(oos);
            SysUtils.close(socket);
            active = false;
        }
    }
}