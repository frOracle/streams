package ru.duc.netfish.views;

import com.wizylab.duck2d.*;
import com.wizylab.duck2d.Window;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class MenuView implements View {
    private KeyListener listener;
    private String name = "vasya";
    private int type = 0;

    @Override
    public void onClose() {
        Window.instance().removeKeyListener(listener);
        listener = null;
    }

    @Override
    public void onTimer(long l) {
        if (Window.instance() != null && listener == null)
            Window.instance().addKeyListener(listener = new MyKeyListener());

        if (Keyboard.onKey(KeyEvent.VK_LEFT)) type = type > 0 ? type - 1 : 4;
        if (Keyboard.onKey(KeyEvent.VK_RIGHT)) type = type < 4 ? type + 1 : 0;


        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);
        if (Keyboard.onKey(KeyEvent.VK_ENTER) && name.length() > 0) {
            Environment.put("name", name);
            Environment.put("type", type);
            Game.show(GameView.class);
        }
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("bg1", 0, 0);
        g.putImage("left", 25, 125);
        g.putImage("fish/" + type, 250, 50);
        g.putImage("right", 575, 125);

        g.setColor(Color.WHITE);
        g.setTextStyle(1, 1, 80);
        g.ctext(0, 450 - 40, 800, 450 + 40, name);
    }

    class MyKeyListener implements KeyListener {
        @Override
        public void keyTyped(KeyEvent e) {
            char ch = e.getKeyChar();
            int code = (int) ch;
            if (name.length() >= 12) return;
            if ((code >= (int) 'A' && code <= (int) 'Z') ||
                    (code >= (int) 'a' && code <= (int) 'z') ||
                    (code >= (int) 'А' && code <= (int) 'Я') ||
                    (code >= (int) 'а' && code <= (int) 'я') ||
                    (code >= (int) '0' && code <= (int) '9'))
                name += ch;
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE && name.length() > 0)
                name = name.substring(0, name.length() - 1);
        }
    }
}
