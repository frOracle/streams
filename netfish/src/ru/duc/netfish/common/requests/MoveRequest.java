package ru.duc.netfish.common.requests;

import java.io.Serializable;

public class MoveRequest implements Serializable {
    private long left, right, up, down;

    public MoveRequest() {
    }

    public MoveRequest(long left, long right, long up, long down) {
        this.left = left;
        this.right = right;
        this.up = up;
        this.down = down;
    }

    public long getLeft() {
        return left;
    }

    public void setLeft(long left) {
        this.left = left;
    }

    public long getRight() {
        return right;
    }

    public void setRight(long right) {
        this.right = right;
    }

    public long getUp() {
        return up;
    }

    public void setUp(long up) {
        this.up = up;
    }

    public long getDown() {
        return down;
    }

    public void setDown(long down) {
        this.down = down;
    }
}