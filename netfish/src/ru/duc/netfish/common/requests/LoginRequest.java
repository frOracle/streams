package ru.duc.netfish.common.requests;

import java.io.Serializable;

public class LoginRequest implements Serializable {
    private String name;
    private int type;

    public LoginRequest(String name, int type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
