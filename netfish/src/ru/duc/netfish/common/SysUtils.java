package ru.duc.netfish.common;

import java.io.Closeable;
import java.io.IOException;

public class SysUtils {
    public static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void close(Closeable closeable) {
        try {
            if (closeable != null) closeable.close();
        } catch (IOException e) {
            // nothing
        }
    }
}