package ru.duc.netfish.common;

import com.wizylab.duck2d.Graph;

import java.awt.*;
import java.io.Serializable;

public class Pickup implements Serializable {
    private double x, y, size;
    private boolean active;
    private int type;

    public Pickup() {
        active = true;
        x = Math.random() * 5000;
        y = Math.random() * 5000;
        type = (int) (10 * Math.random());
        size = 10 + 10 * Math.random();
    }

    public Pickup(Pickup pickup) {
        this.x = pickup.x;
        this.y = pickup.y;
        this.size = pickup.size;
        this.type = pickup.type;
        this.active = pickup.active;
    }

    public Rectangle getHitbox(){
        //return new Rectangle((int) x, (int) y, (int) size, (int) size);
        return new Rectangle((int) x, (int) (y + size / 3 / 2), (int) size, (int) (size / 3 * 2));
    }


    public void draw(Graph g, double sx, double sy) {
        if (!active || x < sx || y < sy || x > sx + 800 || y > sy + 600) return;
        g.putImage("food/" + type, (x - sx), (int) (y - sy), (int) size);
    }

    public double getX() {
        return x;
    }

    public void pickup() {
        active = false;
    }

    public double getY() {
        return y;
    }

    public double getSize() {
        return size;
    }

    public boolean isActive() {
        return active;
    }
}