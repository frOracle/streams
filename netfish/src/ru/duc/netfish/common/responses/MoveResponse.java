package ru.duc.netfish.common.responses;

import ru.duc.netfish.common.Pickup;
import ru.duc.netfish.common.Player;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MoveResponse implements Serializable {
    private Map<Long, Player> players;
    private Set<Pickup> pickups;

    public MoveResponse(Set<Player> players, Set<Pickup> pickups) {
        this.players = new HashMap<>();
        for (Player player : players)
            this.players.put(player.getUUID(), new Player(player));

        this.pickups = new HashSet<>();
        for (Pickup pickup : pickups)
            this.pickups.add(new Pickup(pickup));
    }

    public Map<Long, Player> getPlayers() {
        return players;
    }

    public Set<Pickup> getPickups() {
        return pickups;
    }
}