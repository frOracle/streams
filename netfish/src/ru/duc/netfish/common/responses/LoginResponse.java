package ru.duc.netfish.common.responses;

import java.io.Serializable;

public class LoginResponse implements Serializable {
    private long uuid;

    public LoginResponse(long uuid) {
        this.uuid = uuid;
    }

    public long getUUID() {
        return uuid;
    }
}