package ru.duc.netfish.common;

import com.wizylab.duck2d.Graph;
import ru.duc.netfish.common.requests.MoveRequest;

import java.awt.*;
import java.io.Serializable;

public class Player implements Serializable {
    private double x, y, size;
    private boolean isDead;
    private String name;
    private long uuid;
    private int type;

    public Player(String name, int type) {
        this.uuid = System.currentTimeMillis();
        x = Math.random() * 5000;
        y = Math.random() * 5000;
        this.name = name;
        this.type = type;
        isDead = false;
        size = 15;
    }

    public Player(Player player) {
        this.isDead = player.isDead;
        this.uuid = player.uuid;
        this.size = player.size;
        this.name = player.name;
        this.type = player.type;
        this.x = player.x;
        this.y = player.y;
    }

    public void move(MoveRequest req) {
        if (isDead) return;
        x -= req.getLeft() * (0.05 + 5 / size);
        x += req.getRight() * (0.05 + 5 / size);
        y -= req.getUp() * (0.05 + 5 / size);
        y += req.getDown() * (0.05 + 5 / size);
    }

    public void inc(double size) {
        this.size += size;
    }

    public Rectangle getHitbox() {
        return new Rectangle((int) x, (int) (y + size / 3 / 2), (int) size, (int) (size / 3 * 2));
    }

    public void draw(Graph g, double sx, double sy) {
        if (isDead || x < sx || y < sy || x > sx + 800 || y > sy + 600) return;
        g.putImage("fish/" + type, (x - sx), (int) (y - sy), (int) size);

        g.setTextStyle(1, 1, 12);
        g.setColor(Color.WHITE);
        g.ctext((int) (x + size / 2 - 100 - sx), (int) (y + size / 2 / 3 - 20 - sy), (int) (x + size / 2 + 100 - sx), (int) (y + size / 2 / 3 - sy), name);
    }

    public void kill() {
        isDead = true;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getSize() {
        return size;
    }

    public long getUUID() {
        return uuid;
    }

    public boolean isDead() {
        return isDead;
    }
}