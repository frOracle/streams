package ru.duc.tictactoe.views;

import com.wizylab.duck2d.*;

import java.awt.*;
import java.awt.event.KeyEvent;

public class FinishView implements View {
    private static final String[] NAMES = {"Дружба", "Х", "O"};
    int result;

    @Override
    public void onShow() {
        result = Environment.get("RESULT");
    }

    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE) ||
                Keyboard.onKey(KeyEvent.VK_ENTER) ||
                Mouse.onClick(MouseButton.LEFT))
            Game.show(MenuView.class);
    }

    @Override
    public void onDraw(Graph g) {
        g.setBackground(new Color(255,0,0, 100));
        g.setColor(Color.WHITE);
        g.setTextStyle(4, 1, 30);
        g.ctext(0, 0, 300, 100, "ИГРА ОКОНЧЕНА!");
        g.ctext(0, 100, 300, 150, "ПОБЕДИЛ" + ((result == -1) ? "А" : ""));
        g.setTextStyle(4, 1, 70);
        g.ctext(0, 150, 300, 300, NAMES[result + 1]);
    }
}