package ru.duc.tictactoe.views;

import com.wizylab.duck2d.*;
import ru.duc.tictactoe.AI;
import ru.duc.tictactoe.ais.EasyAI;
import ru.duc.tictactoe.ais.HardAI;

import java.awt.*;
import java.awt.event.KeyEvent;

public class GameView implements View {
    public static final int GRID_SIZE = 3, CELL_SIZE = 100;
    private int[][] grid = new int[GRID_SIZE][GRID_SIZE];
    private int turn;
    private int type;
    private AI ai;

    @Override
    public void onShow() {
        type = Environment.get("GAME_TYPE");
        if (type == 1) ai = new EasyAI();
        if (type == 2) ai = new HardAI();
        for (int y = 0; y < GRID_SIZE; y++)
            for (int x = 0; x < GRID_SIZE; x++) grid[y][x] = 0;
        turn = 0;
    }

    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) Game.show(MenuView.class);
        if (type > 0 && turn % 2 != 0) {
            ai.move(grid);
            next();
        } else if (Mouse.onClick(MouseButton.LEFT)) {
            int x = Mouse.x() / CELL_SIZE, y = Mouse.y() / CELL_SIZE;
            if (grid[y][x] == 0) {
                grid[y][x] = turn % 2 + 1;
                next();
            }
        }
    }

    public void next() {
        if (isWin()) {
            Environment.put("RESULT", turn % 2);
            Game.show(FinishView.class);
        } else if (turn >= GRID_SIZE * GRID_SIZE - 1) {
            Environment.put("RESULT", -1);
            Game.show(FinishView.class);
        } else turn++;
    }

    private boolean isWin() {
        int turn = this.turn % 2 + 1;
        for (int y = 0; y < GRID_SIZE; y++) {
            int hSum = 0, vSum = 0;
            for (int x = 0; x < GRID_SIZE; x++) {
                if (grid[y][x] == turn) hSum++;
                if (grid[x][y] == turn) vSum++;
            }
            if (hSum == GRID_SIZE || vSum == GRID_SIZE) return true;
        }

        int d1Sum = 0, d2Sum = 0;
        for (int i = 0; i < GRID_SIZE; i++) {
            if (grid[i][i] == turn) d1Sum++;
            if (grid[i][GRID_SIZE - 1 - i] == turn) d2Sum++;
        }
        return (d1Sum == GRID_SIZE || d2Sum == GRID_SIZE);
    }

    @Override
    public void onDraw(Graph g) {
        g.setColor(Color.YELLOW);

        g.line(100, 0, 100, 300);
        g.line(200, 0, 200, 300);
        g.line(0, 100, 300, 100);
        g.line(0, 200, 300, 200);

        g.setColor(Color.WHITE);
        g.setTextStyle(4, 1, 60);
        for (int y = 0; y < GRID_SIZE; y++) {
            for (int x = 0; x < GRID_SIZE; x++) {
                if (grid[y][x] == 0) continue;
                g.ctext(0 + 100 * x, 0 + y * 100, 100 + 100 * x, 100 + 100 * y, (grid[y][x] == 1) ? "X" : "O");
            }
        }
    }
}