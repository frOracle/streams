package ru.duc.tictactoe.views;

import com.wizylab.duck2d.*;

import java.awt.*;
import java.awt.event.KeyEvent;

public class MenuView implements View {
    private String[] PLAYERS = {"Человек", "Компьютер 1", "Компьютер 2"};
    private int type = 0;

    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);
        if (Keyboard.onKey(KeyEvent.VK_ENTER) || Mouse.onClick(MouseButton.LEFT)) {
            Environment.put("GAME_TYPE", type);
            Game.show(GameView.class);
        }
        if (Keyboard.onKey(KeyEvent.VK_RIGHT)) type = type < 2 ? type + 1 : 0;
        if (Keyboard.onKey(KeyEvent.VK_LEFT)) type = type > 0 ? type - 1 : 2;
    }

    @Override
    public void onDraw(Graph g) {
        g.setBackground(new Color(0, 0, 255, 100));

        g.setColor(Color.WHITE);
        g.setTextStyle(4, 1, 40);
        g.ctext(0, 0, 300, 50, "Крестики");
        g.ctext(0, 50, 300, 100, "&");
        g.ctext(0, 100, 300, 150, "Нолики");

        g.setColor(Color.WHITE);
        g.setTextStyle(4, 1, 20);
        g.ctext(0, 170, 300, 200, "ВАШ ПРОТИВНИК");
        g.ctext(0, 200, 300, 230, PLAYERS[type]);

        g.setColor(Color.WHITE);
        g.setTextStyle(4, 1, 20);
        g.ctext(0, 250, 300, 300, "Что-нибудь нажмите!");
    }
}