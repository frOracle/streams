package ru.duc.tictactoe;

public interface AI {
    void move(int[][] grid);
}