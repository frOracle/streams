package ru.duc.tictactoe;

import com.wizylab.duck2d.Environment;
import com.wizylab.duck2d.Game;
import ru.duc.tictactoe.views.FinishView;
import ru.duc.tictactoe.views.GameView;
import ru.duc.tictactoe.views.MenuView;

public class Run {
    public static void main(String[] args) {
        Environment.put("window.title", "Крестики & Нолики");
        Environment.put("window.width", GameView.GRID_SIZE * GameView.CELL_SIZE);
        Environment.put("window.height", GameView.GRID_SIZE * GameView.CELL_SIZE);
        Game.addView(new MenuView(), new GameView(), new FinishView());
        Game.start(MenuView.class);
    }
}