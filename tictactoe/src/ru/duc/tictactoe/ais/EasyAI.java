package ru.duc.tictactoe.ais;

import ru.duc.tictactoe.AI;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

import static ru.duc.tictactoe.views.GameView.GRID_SIZE;

public class EasyAI implements AI {
    @Override
    public void move(int[][] grid) {
        ArrayList<Point> turns = new ArrayList<>();
        for (int y = 0; y < GRID_SIZE; y++)
            for (int x = 0; x < GRID_SIZE; x++)
                if (grid[y][x] == 0) turns.add(new Point(x, y));
        if (turns.size() < 1) return;
        Collections.shuffle(turns);
        Point turn = turns.get(0);
        grid[turn.y][turn.x] = 2;
    }
}