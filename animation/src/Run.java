import com.wizylab.duck2d.Game;
import com.wizylab.duck2d.Graph;
import com.wizylab.duck2d.Keyboard;
import com.wizylab.duck2d.View;

import java.awt.event.KeyEvent;

public class Run implements View {
    private double x = 100, y = 100, frame = 0;
    private int dir = 0;

    public static void main(String[] args) {
        Game.start(new Run());
    }

    @Override
    public void onTimer(long l) {
        if (Keyboard.onKey(KeyEvent.VK_ESCAPE)) System.exit(0);
        if (Keyboard.hasKey(KeyEvent.VK_LEFT)) {
            x -= 0.1 * l;
            dir = 0;
        }
        if (Keyboard.hasKey(KeyEvent.VK_RIGHT)) {
            x += 0.1 * l;
            dir = 1;
        }
        frame += 0.01 * l;
        if (frame > 6) frame = 0;
    }

    @Override
    public void onDraw(Graph g) {
        g.putImage("player/" + dir + "/" + (int) frame, x, y);
    }
}